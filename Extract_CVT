-- Program Name: Rewrite CVT Extract.sql
-- Folder: MyVAAccess\TeleHealth\
-- Purpose: Exgtract TeleHealth CVT encounters and unique patients information
-- Note: Replace Visit table with Workload table per Adam, 08/11/2017
	  -- Revised by adding Credit Stop 490, 699, 679, 723, 724, 09/05/2017 
	  -- Update to bring in Sta6a from Division table, instead of PatientPCP table,  09/05/2017.
-- TeleRetinal Imaging (718) was not included: Primary stop of TeleRetinal Imaging (718) with any credit stop, were not typically included as CVT or SFT */ 

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tele_visits') IS NOT NULL
  DROP TABLE #tele_visits;

DECLARE @index_date DATETIME2(0)
       ;
SET @index_date = CONVERT(DATETIME2(0),CONVERT(DATE,'2017-04-30'));
PRINT CONVERT(VARCHAR(20),@index_date,121) + ' is the INDEX DATE';

SELECT
 tv.Sta3n
,tv.PatientSID
,CONVERT(VARCHAR(50),NULL)  AS PatientICN
,CONVERT(VARCHAR(50),NULL)  AS ScrSSN
,tv.VisitSID
,tv.VisitDateTime
,tv.PrimaryStopCodeSID
,tv.SecondaryStopCodeSID
,tv.ServiceCategory
--,tv.WorkloadLogicFlag
,tv.LocationSID
,loc.LocationType
,loc.PrimaryStopCodeSID     AS loc_PrimaryStopCodeSID  
,loc.SecondaryStopCodeSID   AS loc_SecondaryStopCodeSID
,psc.StopCode               AS PrimaryStopCode
,psc.StopCodeName           AS PrimaryStopCodeName
,ssc.StopCode               AS SecondaryStopCode 
,ssc.StopCodeName           AS SecondaryStopCodeName
,loc.LocationName          
,CONVERT(INT,0)             AS ExclusionFlag --0 keep; 1 test patient; 2...
, QTR = CASE
         WHEN DATEPART(Month, VisitDateTime) BETWEEN 10 AND 12 THEN 1
         WHEN DATEPART(Month, VisitDateTime) BETWEEN 1 AND 3 THEN 2
         WHEN DATEPART(Month, VisitDateTime) BETWEEN 4 AND 6 THEN 3
		 WHEN DATEPART(Month, VisitDateTime) BETWEEN 7 AND 9 THEN 4
     END 

, FY = CASE 
	     WHEN DATEPART(Month, VisitDateTime) BETWEEN 10 AND 12 THEN ( DATEPART(Year, VisitDateTime) +1)
		 WHEN DATEPART(Month, VisitDateTime) BETWEEN 1 AND 3 THEN  DATEPART(Year, VisitDateTime)
         WHEN DATEPART(Month, VisitDateTime) BETWEEN 4 AND 6 THEN  DATEPART(Year, VisitDateTime)
		 WHEN DATEPART(Month, VisitDateTime) BETWEEN 7 AND 9 THEN  DATEPART(Year, VisitDateTime)
      END 

, STa6a = CASE
				when c.sta6a is not null OR c.sta6a <> '*Missing*'
				then c.sta6a 
				else CAST(tv.sta3n as varchar(5)) 
		  End 
INTO
  #tele_visits
FROM
  CDWWork.Outpat.Workload AS tv   --workload table in stead of visit table
  JOIN CDWWork.Dim.Location AS loc ON ( loc.LocationSID = tv.LocationSID and loc.Sta3n = tv.sta3n
                    ) 
  join CDWWork.Dim.Division as c
		on (tv.DivisionSID = c.DivisionSID)
--	join CDWWork.Dim.Division as d
--		on (loc.DivisionSID = d.DivisionSID) 
		
  JOIN CDWWork.Dim.StopCode AS psc ON (     psc.StopCodeSID = loc.PrimaryStopCodeSID
								 )
  JOIN CDWWork.Dim.StopCode AS ssc ON (     ssc.StopCodeSID = loc.SecondaryStopCodeSID 
                                        AND ssc.StopCode IN ( 179 --Real Time Clinical Video Care to Home.  
                                                             ,136 --TelePost Deployment Integrated Care Patient
                                                             ,137 --TelePost Deployment Integrated Care Provider
                                                             ,440 --TeleFitting Adjustments
                                                             ,444 --TeleComp & Pen Patient
                                                             ,445 --TeleComp & Pen Provider
                                                             ,446 --TeleIDES (Disability Eval) Patient
                                                             ,447 --TeleIDES Provider
															 ,490
                                                             ,491 --TeleTransplant 
                                                             ,644 --National Center Clinical Video Telehealth Real Time Patient
                                                             ,645 --National Center Clinical Video Telehealth Real Time Provider
                                                             ,648 --Clinical Video Telehealth to Non-VAMC Location.
                                                             ,690 --Clinical Video Telehealth Real Time Patient
                                                             ,692 --Clinical Video Telehealth Real Time Same Station Provider
                                                             ,693 --Clinical Video Telehealth Real Time Different Station Provider
                                                             ,708 --TeleSmoking Cessation
															 ,699, 679, 723, 724  /* new for FY16 17 */
                                                            )
                    )
WHERE
      tv.VisitDateTime BETWEEN DATEADD(YEAR,-5, @index_date) AND @index_date;
PRINT CONVERT(VARCHAR(20),@@ROWCOUNT) + ' Tele visits recorded.';  
-- 2017-04-30 00:00:00 is the INDEX DATE
-- 6,635,766 Tele visits recorded.


CREATE INDEX tele_visits_idx1 ON #tele_visits ( PatientSID, VisitDateTime, PrimaryStopCodeSID, SecondaryStopCodeSID );

select top 100 * from #tele_visits

--Flag any test patients
UPDATE
  #tele_visits
SET
  #tele_visits.PatientICN    = CASE WHEN ISNULL(spat.PatientICN,'*Unknown at this time*') NOT IN ( '*Missing*', '*Unknown at this time*' ) THEN spat.PatientICN ELSE NULL END
 ,#tele_visits.ScrSSN        = CASE WHEN ISNULL(spat.ScrSSN,'*Unknown at this time*')     NOT IN ( '*Missing*', '*Unknown at this time*' ) THEN spat.ScrSSN     ELSE NULL END 
 ,#tele_visits.ExclusionFlag = CASE WHEN ISNULL(spat.TestPatientFlag,'N') IN ( '*', 'Y' )
                                         OR
  ISNULL(spat.CDWPossibleTestPatientFlag,'N') = 'Y'
       THEN 1
                  --
       WHEN ISNULL(spat.PatientICN,'*Unknown at this time*') IN ( '*Missing*', '*Unknown at this time*' )
              OR
       ISNULL(spat.ScrSSN,'*Unknown at this time*')     IN ( '*Missing*', '*Unknown at this time*' )
         THEN 2
         ELSE 0
         END
FROM
	#tele_visits  
JOIN CDWWork.SPatient.SPatient AS spat ON ( spat.PatientSID = #tele_visits.PatientSID );

PRINT CONVERT(VARCHAR(20),@@ROWCOUNT) + 'Tele visits updated.';  

SET NOCOUNT OFF;
--In Visit table for FY16:
		--The result is that I retrieved 1,789,268 records and yours only retrieved 87 
		--For patient information, I found 1,677 being test patients and only 1 of these had a NULL for ScrSSN – all had a value for PatientICN.

-- In Workload table:
		-- 0 invalid SCRSSN
		-- ExclusionFlag	VisitCout	CountSCRSSN
		--			1			   76			 52
		--			0		1,580,026		307,070

--2017-04-30 00:00:00 is the INDEX DATE
--6,635,766 Tele visits recorded.

SELECT
  *
FROM
  #tele_visits
WHERE
      ExclusionFlag IN ( 1, 2 )
  AND ( PatientICN IS NULL OR ScrSSN IS NULL )
ORDER BY
  ExclusionFlag
-- no patients without patient ICN or SSN, 09/12/2017.

SELECT
  *
FROM
  #tele_visits
WHERE
      ( PatientICN IS NULL OR ScrSSN IS NULL )
--none

select distinct ExclusionFlag, count(*) as VisitCout, count(Distinct SCRSSN) as CountSCRSSN
 from #tele_visits  
 group by ExclusionFlag
 
/* FY16 from Visit table:
						 ExclusionFlag	 VisitCout	CountSCRSSN
									0	 1,787,592		341,316
									1		 1,677			588   
				
** Workload table , RUN 09/12/2017: 
					ExclusionFlag	VisitCout	CountSCRSSN
								1		  386			181
								0	6,635,380	     833,423 */
 select top 100 * 
 from #tele_visits
 where ExclusionFlag=0  

--Join in PCMM cohort to flag primary care patient user, exclude test patient, link in VISN and District, create CY  
USE PACT_CC;
GO

DROP TABLE #CVT_JOIN_PCPS

SELECT distinct 
	  CVT.*
	, Datepart(MONTH, CVT.VisitDateTIme) as Month
	, CASE 
			WHEN VisitDateTime IS NOT NULL THEN DATEPART(Year, VisitDateTime) 
			ELSE NULL
		END as CY
	, Case when SCRSSN_NUM is not null then 1
		Else 0
	  End as PCMM_User
	, DIM.VISN
	, DIM.DISTRICT
	, DIM.CategoryName
INTO #CVT_JOIN_PCPS
FROM #tele_visits AS CVT  
Left JOIN [PACT_CC].[econ].[PatientPCP] AS PCPTs

ON PCPTS.FY=CVT.FY
  AND PCPTS.QTR= CVT.QTR
  AND PCPTS.SCRSSN_num = CVT.SCRSSN

LEFT JOIN [PACT_CC].[Dim].[DimVISNFacility6a] AS DIM 
 ON CVT.STA6A =DIM.STA6AID
--FOR VISN AND dISTRCT

WHERE CVT.ExclusionFlag =0
/* 6635380 row(s) affected */

SELECT TOP 200 * FROM #CVT_JOIN_PCPS

/* Query for unique user and PCMM user count  */
Select  
	FY
	,  count(SCRSSN) AS AllCVT_Encs
	,  SUM (PCMM_User) AS PCMM_Encs
	,  Count(DISTINCT ScrSSN) AS Uniq_USER
	, (SUM(PCMM_User )* 100 /Count(SCRSSN) ) as Perc
From #CVT_JOIN_PCPS
GROUP BY FY
/* 
  FY  AllCVT_Encs	  PCMM_Encs	 Uniq_USER	Perc
2012	  346,033		334,958		92,623	96
2013	1,029,226		999,718	   201,710	97
2014	1,256,568	  1,207,726	   247,986	96
2015	1,451,645	  1,403,152	   282,740	96
2016	1,595,770	  1,528,114	   307,993	95
2017	  956,138		      0	   228,451	0              */

CREATE CLUSTERED INDEX idx_FYQTR
on #CVT_JOIN_PCPS (FY, scrssn, VisitDateTime, PrimaryStopCode );
go

DROP TABLE Tele.CVT_FY13_17 

/* Load the encounter table ontp Tele schema */
select * into Tele.CVT_FY13_17 
from #CVT_JOIN_PCPS 

USE OABI_MyVAAccess

SELECT top 20 * from Tele.CVT_FY13_17 



-- ENCOUNTER and patient counts by sta6a by month 
Select distinct 
	CY
	, Month
	, Sta6a
	, PCMM_User
	, count(*) AS Visit_Count
	, count(distinct SCRSSN) AS Patient_Count
From Tele.CVT_FY13_17 
GROUP BY CY
	, Month
	, Sta6a
	, PCMM_User
ORDER BY CY
	, Month
	, Sta6a
	, PCMM_User

--CREATE unique patient count per year
Select distinct 
	CY
	, count(distinct SCRSSN) AS Patient_Count
From Tele.CVT_FY13_17 
GROUP BY CY
ORDER BY CY
/*	CY	Patient_Count
	2012	128066
	2013	214180
	2014	258748
	2015	287974
	2016	313860
	2017	164694      */

Select distinct LocationType, count(*) as CVTEntCount          
	From Tele.CVT_FY13_17 
	Group by LocationType
	Order by CVTEntCount Desc

Select distinct LocationName, count(*) as CVTEntCount                    
	From Tele.CVT_FY13_17 
	Group by LocationName
	Order by CVTEntCount Desc
	          
Select distinct PrimaryStopCode, PrimaryStopCodeName, count(*) as CVTEntCount          
	From Tele.CVT_FY13_17
	Group by PrimaryStopCode, PrimaryStopCodeName 
	Order by CVTEntCount Desc

Select distinct SecondaryStopCode, SecondaryStopCodeName, count(*) as CVTEntCount          
	From Tele.CVT_FY13_17
	Group by SecondaryStopCode, SecondaryStopCodeName
	Order by CVTEntCount Desc


--PAIRING ENCOUNTERS TO BE VISITS  --------------------------------------------------------------------------------------
SELECT COUNT(*) FROM #CVT_JOIN_PCPS 
--6,635,380 row(s) affected

SELECT TOP 100 * FROM #CVT_JOIN_PCPS 

DROP TABLE #PAIRED_CVT

SELECT DISTINCT 
	CY, 
	Month, 
	Datepart(DAY, VisitDateTime) AS Day, 
	scrssn,
	VISN,
	PrimaryStopCode ,
	PrimaryStopCodeName

INTO #PAIRED_CVT
from #CVT_JOIN_PCPS 
order by VISN, SCRSSN, CY, MONTH, DAY, PrimaryStopCode


select top 100 * from #PAIRED_CVT

DROP TABLE Tele.PAIRED_CVT
select * into Tele.PAIRED_CVT from #PAIRED_CVT

select top 100 * from Tele.PAIRED_CVT

select distinct 
	 VISN
    ,CY
	,MONTH
	,COUNT(*) AS VisitCount 
FROM Tele.PAIRED_CVT
GROUP BY VISN, CY, MONTH
ORDER BY VISN, CY, MONTH


select distinct 
	CY
	, PrimaryStopcode
	, PrimaryStopCodeName
	, COUNT(*) AS VisitCount 
FROM Tele.PAIRED_CVT
GROUP BY CY
		, PrimaryStopcode
		, PrimaryStopCodeName
ORDER BY CY
		, COUNT(*) desc
		, PrimaryStopcode
		, PrimaryStopCodeName
